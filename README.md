# Flutter: Widgets

This is a simple application with State Management

### Version: 1.0.0


| Image 1    | Image 2     | Image 3     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649199/states/9_bqxa5n.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649193/states/7_j4yydq.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649193/states/4_mw6qn6.png" width="250"> |

| Image 4    | Image 5     | Image 6     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649192/states/1_ijmeah.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649192/states/3_lwku1a.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649192/states/2_i93daq.png" width="250"> |

| Image 7    | Image 8     | Image 9     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649194/states/8_ug7tdz.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649193/states/5_iwrl11.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649513/10_v3mhwt.png" width="250"> |


### Usage

```js
git clone https://gitlab.com/Developerkimaiyo/flutter_state_management.git

```


### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com



---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.