import 'package:flutter/material.dart';
import 'package:flutter_project_2/model/stack.dart';
import 'package:provider/provider.dart';

import 'detail.dart';

class GridItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;
  final String url;
  GridItem(this.id,this.title, this.color,this.url);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      child: GridTile(
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(
              DetailScreen.routeName,
              arguments:id,
            );
          },
          child: Image.network(
             url,
            fit: BoxFit.cover,
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,

          title: Text(
            title,
            textAlign: TextAlign.center,
          ),

        ),
      ),
    );
  }
}