
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/stacks.dart';

class DetailScreen extends StatelessWidget {
  static const routeName = '/detail';

  @override
  Widget build(BuildContext context) {
    final stackId =
    ModalRoute.of(context).settings.arguments as String; // is the id!
    final loadedStack = Provider.of<Stacks>(
      context,
      listen: false,
    ).findById(stackId);
    return Scaffold(
      appBar: AppBar(
        title: Text(loadedStack.title),
      ),
      body: SingleChildScrollView(
        child: Column(

          children: <Widget>[

            Container(
              height: 300,
              width: double.infinity,
              padding: const EdgeInsets.all(30.0),
              child: Image.network(
                loadedStack.url,
                fit: BoxFit.cover,
              ),
            ),

            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              child: Text(
                loadedStack.title,
                textAlign: TextAlign.center,
                softWrap: true,
                  style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Colors.black,
                      fontSize: 22,
                      fontWeight: FontWeight.w600
                  )
              ),

            )
          ],
        ),
      ),
    );
  }
}