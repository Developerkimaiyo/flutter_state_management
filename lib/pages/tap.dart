import 'package:flutter/material.dart';


import 'form.dart';
import 'grid.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      // initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          title: Text('GoRider'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
              icon: Icon(
                Icons.star,
              ),
              text: 'FormPage',
            ),
              Tab(
                icon: Icon(
                  Icons.category,
                ),
                text: 'GridPage',
              ),

            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            FormScreen(),
            GridPage(),

          ],
        ),
      ),
    );
  }
}