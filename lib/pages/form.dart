import 'package:flutter/material.dart';
import 'package:flutter_project_2/providers/formProvider.dart';
import 'package:provider/provider.dart';


import 'formItem.dart';

  class FormScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
  }

  class _FormScreenState extends State<FormScreen> {
  String _chosenValue;
  static const routeName = '/formScreen';

  String id ="1";
  String title = "Great";
  Color color;

  TextEditingController nameController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {

    void selectCategory(name,stack) {
      // context.read<FormProvider>().setData(name,stack);
      Navigator.of(context).pushNamed(
        FormItem.routeName,
        arguments:{
          'stack':stack,
          'name':name
        },
      );
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,

      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Card(
            elevation: 5,
            
            child: SingleChildScrollView(
              child: Container(

                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,

                  children: <Widget>[

                     TextField(
                       controller: nameController,
                      decoration: new InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Enter Name",
                        fillColor: Colors.white,
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                          fontFamily: "Poppins",
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600
                      ),

                    ),
                      SizedBox(height: 20),
                      DropdownButton<String>(
                        isExpanded: true,
                        value: _chosenValue,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(
                            fontFamily: "Poppins",
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                        underline: DropdownButtonHideUnderline(child: Container()),
                        items: <String>[
                          'flutter',
                          'adonis',
                          'go',
                          'quarkus',
                          'vue',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        hint: Text(
                          "Please choose a language",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },

                    ),
                    SizedBox(height: 10),
                    TextButton.icon(
                        label: Text('View'),
                        icon: Icon(Icons.remove_red_eye),

                        onPressed: () {
                          if(nameController.text!=null && _chosenValue !=null ){
                            selectCategory(nameController.text,_chosenValue);
                          }else {
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Field is Empty'),
                                )
                            );
                          }
                        }
                    )

                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


}