import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/stacks.dart';
import 'package:flutter_project_2/providers/formProvider.dart';


class FormItem extends StatelessWidget {

  static const routeName = '/formItem';
  @override
  Widget build(BuildContext context) {
    // var formstate = context.watch<FormProvider>();
    final routeArgs =
     ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final name = routeArgs['name'];
    final stack = routeArgs['stack'];
    final loadedStack = Provider.of<Stacks>(
      context,
      listen: false,
    ).findByStck(stack);



    return Scaffold(
      appBar: AppBar(
        title: Text(name),
      ),
      body:  SingleChildScrollView(

      child: Card(

        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,

        ),
        child: Padding(
          padding: EdgeInsets.all(50),

          child: ListTile(
            leading: CircleAvatar(
                child: FittedBox(
                  child: Image.network(loadedStack.url, fit: BoxFit.cover),
                ),

            ),
            title: Text(loadedStack.title),
            subtitle: Text(loadedStack.stck),
            trailing: Text(loadedStack.id),
          ),
        ),
      ),

      ),
    );
  }
}