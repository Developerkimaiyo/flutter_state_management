import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/stacks.dart';
import 'gridItem.dart';

class GridPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final stackData = Provider.of<Stacks>(context);
    final stacks =   stackData.items;
    return Scaffold(
      body: GridView.builder(
        padding: const EdgeInsets.all(20),
        itemCount: stacks.length,
        itemBuilder: (ctx, i) => GridItem(
          stacks[i].id,
          stacks[i].title,
          stacks[i].color,
          stacks[i].url,

        ),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
      ),
    );
  }
}