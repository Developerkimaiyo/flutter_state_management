import 'package:flutter/material.dart';

class StackModel with ChangeNotifier {
  final String id;
  final String stck;
  final String title;
  final Color color;
  final String url;
  bool isFavorite;

  StackModel({
    @required this.id,
    @required this.stck,
    @required this.title,
    @required this.url,
    this.color = Colors.orange,
    this.isFavorite = false,
  });
  void toggleFavoriteStatus() {
    isFavorite = !isFavorite;
    notifyListeners();
  }
}