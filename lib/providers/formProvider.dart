import 'package:flutter/material.dart';

class FormProvider with ChangeNotifier{
  String _name='';
  String _stack='';

  String get name => _name;
  String get stack => _stack;


  setData(name, stack){
    _name = name;
    _stack = stack;
    notifyListeners();
  }


}
