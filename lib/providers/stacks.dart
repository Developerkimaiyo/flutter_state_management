import 'package:flutter/material.dart';

import '../model/stack.dart';

class Stacks with ChangeNotifier {
  List<StackModel> _items = [
    StackModel(
      id: '1',
      stck:'flutter',
      title: 'Flutter - Mobile',
      url:'https://res.cloudinary.com/dwlhubxxu/image/upload/v1619608748/languages/flutter_rj1ydc.jpg',
      color: Colors.red,
    ),
    StackModel(
      id: '2',
      stck:'vue',
      title: 'Vue - Frontend',
      url:'https://res.cloudinary.com/dwlhubxxu/image/upload/v1619608748/languages/vue_kw0s9e.jpg',
      color: Colors.orange,
    ),
    StackModel(
      id: '3',
      stck:'adonis',
      title: 'adonis - backend',
      url:'https://res.cloudinary.com/dwlhubxxu/image/upload/v1619608748/languages/adonis_o5tylo.jpg',
      color: Colors.amber,
    ),
    StackModel(
      id: '4',
      stck:'quarkus',
      title: 'quarkus - backend',
      url:'https://res.cloudinary.com/dwlhubxxu/image/upload/v1619608748/languages/quarkus_nb1wku.png',
      color: Colors.blue,
    ),
    StackModel(
      id: '5',
      stck:'go',
      title: 'go - backend',
      url:'https://res.cloudinary.com/dwlhubxxu/image/upload/v1619608748/languages/go_bzbhkd.jpg',
      color: Colors.green,
    ),
  ];
  // var _showFavoritesOnly = false;

  List<StackModel> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._items];
  }


  StackModel findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }
  StackModel findByStck(String stck) {
    return _items.firstWhere((prod) => prod.stck == stck);
  }

  void addStack() {
    // _items.add(value);
    notifyListeners();
  }
}