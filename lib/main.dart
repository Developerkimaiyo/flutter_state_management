import 'package:flutter/material.dart';
import 'package:flutter_project_2/pages/detail.dart';
import 'package:flutter_project_2/pages/form.dart';
import 'package:flutter_project_2/pages/formItem.dart';
import 'package:flutter_project_2/pages/grid.dart';
import 'package:flutter_project_2/pages/tap.dart';
import 'package:provider/provider.dart';
import './providers/stacks.dart';
void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => Stacks(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'GoRider',
        theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.amber,
          canvasColor: Color.fromRGBO(255, 254, 229, 1),
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline1: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              )),
        ),
        initialRoute: '/', // default is '/'
        routes: {
          '/': (ctx) => TabsScreen(),
          '/FormPage': (ctx) => FormScreen(),
          '/GridPage': (ctx) => GridPage(),
          '/detail':(ctx) => DetailScreen(),
          '/formItem':(ctx) => FormItem()
        },
        // ignore: missing_return
        onGenerateRoute: (settings) {
          print(settings.arguments);
        },
        onUnknownRoute: (settings) {
          return MaterialPageRoute(builder: (ctx) => FormScreen(),);
        },
      ),
    );
  }
}